package com.core.doGood.userManagement.producer;

import com.core.doGood.userManagement.dbModel.DbUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class Sender {

    private static final Logger LOGGER = LoggerFactory.getLogger(Sender.class);
    private CountDownLatch latch = new CountDownLatch(1);

    @Autowired
    private KafkaTemplate<String, DbUser> simpleKafkaTemplate;


    public void send(String topic, DbUser payload) {
        LOGGER.info("sending payload='{}' to topic='{}'", payload, topic);
        this.simpleKafkaTemplate.send(topic, payload);
    }

}
