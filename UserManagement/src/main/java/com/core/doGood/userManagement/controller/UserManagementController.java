package com.core.doGood.userManagement.controller;

import com.core.doGood.userManagement.model.User;
import com.core.doGood.userManagement.service.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/UserController")
public class UserManagementController {

    @Autowired
    private UserManagementService userManagementService;

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PostMapping(value = "/insertUser")
    public ResponseEntity insertUser(@RequestBody User user) {
        User newUser = userManagementService.saveOrUpdate(user);
        return new ResponseEntity("Usuario insertado",HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteUser")
    public ResponseEntity deleteUser(@RequestBody User user) {
        userManagementService.delete(user);
        return new ResponseEntity("Usuario borrado",HttpStatus.OK);
    }
}
