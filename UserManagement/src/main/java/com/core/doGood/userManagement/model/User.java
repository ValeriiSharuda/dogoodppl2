package com.core.doGood.userManagement.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private int userId;
    private String email;
    private String password;
    private String name;
    private String lastName;
    private Boolean active;
    private String address;
    private String picture;
    private String status;
    private int postalCode;
    private Date birthDate;
    private String phone;
}
