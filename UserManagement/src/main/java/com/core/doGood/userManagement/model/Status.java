package com.core.doGood.userManagement.model;

public enum Status {
    NONACTIVE,
    ACTIVE,
    WAITINGFORPASSWORD;
}
