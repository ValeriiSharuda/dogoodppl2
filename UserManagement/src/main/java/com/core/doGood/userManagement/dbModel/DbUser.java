package com.core.doGood.userManagement.dbModel;

import com.core.doGood.userManagement.model.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "user")
public class DbUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "userId")
    private int userId;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "name")
    private String name;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "address")
    private String address;

    @Column(name = "picture")
    private String picture;

    @Column(name = "status")
    private Status status = Status.NONACTIVE;

    @Column(name = "postalCode")
    private int postalCode;

    @Column(name = "birthDate")
    private Date birthDate;

    @Column(name = "phone")
    private String phone;

}