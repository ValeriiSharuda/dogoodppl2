package com.core.doGood.userManagement.mapper;

import com.core.doGood.userManagement.dbModel.DbUser;
import com.core.doGood.userManagement.model.User;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class UserMapper {

    public User from(DbUser dbUser) {
        User user = new User();
        BeanUtils.copyProperties(dbUser, user);

        return user;
    }

    public DbUser toDbModel (User user) {
        DbUser dbUser = new DbUser();
        BeanUtils.copyProperties(user, dbUser);

        return dbUser;
    }

}
