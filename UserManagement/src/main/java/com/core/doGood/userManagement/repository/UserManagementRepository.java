package com.core.doGood.userManagement.repository;

import com.core.doGood.userManagement.dbModel.DbUser;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserManagementRepository extends CrudRepository<DbUser,Integer> {

    void removeByEmail(String email);
}
