package com.core.doGood.userManagement.service;

import com.core.doGood.userManagement.dbModel.DbUser;
import com.core.doGood.userManagement.mapper.UserMapper;
import com.core.doGood.userManagement.model.User;
import com.core.doGood.userManagement.producer.Sender;
import com.core.doGood.userManagement.repository.UserManagementRepository;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserManagementService {

    @Autowired
    private UserManagementRepository userManagementRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private Sender sender;

    @Value("${spring.kafka.topic.userRegistered}")
    private String KAFKAEMAIL_TOPIC;

    @SneakyThrows
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<User>();
        userManagementRepository.findAll().forEach(user -> users.add(userMapper.from(user)));
        return users;
    }

    @SneakyThrows
    public User getUserById(int id) {
        DbUser dbUser = userManagementRepository.findById(id).get();
        return userMapper.from(dbUser);
    }

    @SneakyThrows
    public User saveOrUpdate(User user) {
        DbUser dbUser = userMapper.toDbModel(user);
        DbUser createdUser = userManagementRepository.save(dbUser);
        sender.send(KAFKAEMAIL_TOPIC, createdUser);
        return user;
    }

    @SneakyThrows
    public void delete(User user) {
        userManagementRepository.removeByEmail(user.getEmail());
        sender.send(KAFKAEMAIL_TOPIC, null);
    }
}
