package com.core.doGood.userManagement.mapper;

import com.core.doGood.userManagement.dbModel.DbUser;
import com.core.doGood.userManagement.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class UserMapperTest {

    @InjectMocks
    private UserMapper userMapper;


    @Test
    public void ToDbmodelMapper_shouldMappADefaultUser() {
        User user = new User();
        user.setUserId(1);
        user.setName("Alberto");
        user.setLastName("Garcia");
        user.setPhone("+341279525");

        DbUser dbUser = userMapper.toDbModel(user);

        assertThat(user.getUserId(), is(dbUser.getUserId()));
        assertThat(user.getName(), is(dbUser.getName()));
        assertThat(user.getLastName(), is(dbUser.getLastName()));
        assertThat(user.getPhone(), is(dbUser.getPhone()));
    }

}
