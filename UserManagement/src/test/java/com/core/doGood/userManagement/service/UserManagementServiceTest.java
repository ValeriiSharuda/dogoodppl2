package com.core.doGood.userManagement.service;
import com.core.doGood.userManagement.dbModel.DbUser;
import com.core.doGood.userManagement.mapper.UserMapper;
import com.core.doGood.userManagement.model.User;
import com.core.doGood.userManagement.producer.Sender;
import com.core.doGood.userManagement.repository.UserManagementRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.core.KafkaTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)

public class UserManagementServiceTest<SimpleKafkaTemplate> {

    @InjectMocks
    private UserManagementService userManagementService;

    @Mock
    private UserMapper userMapper;

    @Mock
    private UserManagementRepository userManagementRepository;

    @Mock
    private Sender sender;

    @Mock
    private KafkaTemplate kafkaTemplate;

    @Test
    public void save_shouldBeSave_whenGivenActiveUser() {

       /* KafkaTemplate<String, DbUser> simpleKafkaTemplate = new KafkaTemplate<String,DbUser>();
        when(simpleKafkaTemplate.send(anyString(),any(DbUser.class))).thenReturn()
        when(userMapper.toDbModel(any(User.class))).thenReturn(new DbUser());
        userManagementService.saveOrUpdate(new User());

        verify(userManagementRepository).save(any(DbUser.class));
        verify(userMapper).toDbModel(any(User.class));*/
    }

}
