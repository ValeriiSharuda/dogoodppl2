<#setting locale="es_ES">
<html>
<head>
    <style>
    th {
      text-align: left;
    }

    .list th {
      background: #d9d9d9;
    }

    .list table, .list th, .list td {
      padding: 5px;
      border: 1px solid black;
      border-collapse: collapse;
    }

    .bankInfo table, .bankInfo th, .bankInfo td {
      padding: 5px;
      background: #d9d9d9;
    }
  </style>
</head>
<body>
<img
        src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCABIAPoDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9UicVShupH1C6hZGEaBCjY4ORzzS6xHcyafKLNwlzjKbuhI/hPoD0z2zXP6Lpel+INOS8SS7ZmJV1kuXDxupwyMAeCCMV4uOq4uNWnHCwTtdu8uVNWat8Mu6fyRrTdPVVHb5X/VHQWt4013dxlCqQlVDEYySMmrENzHMzqhyUba3sfSsQ6FdWXzaffyr/ANMbk+ah9snkfgaNN1L7PDLbizaC+jO42wbO/J5ZWPUZPWuKjmNahONLGwcG+Z33T6qMXHql0aUnbSL1ttKlGS5qbvt/T/qx0FFRwM5jHm7d/fb0qSvpovmSZyBRSUtMAoopKAFooooAKKKSgBaKSloAKKKKACiiigAooooAKKKKACiiigAopMiloAKKKKACiiigBGG6vLfGmrP8K/F0HiAhm8N6s62+povS3m4CTge4G1vXAPWvU65P4o2+mXXgLXI9XlSGya2fMj/wtj5SPfdjFebmFOUqDnB2lH3k/Nd/J7PyZjVT5W1ujltN8S+LI/jI9jLEb3wjqFv51pcRxgxxgIDkOO5bgg+oxXoWqad9sVZY2Ed3Cd0Unoe4Psehr5V+D/xE+IEEY0Pw9ax6zbx42rdIStsDn+PcML14PpxX0F4Xum0H+0ZPEmvw3murCtzeRRttitYedoROy5zyeSfwr5jLcbRzLDShVUnGbbbloove0ZdbNXjbVb6WIwNSdX3qabe/p5enQ6qxM14wuDLsiIx5AXlW75PfnNeeftB/Fy5+E/hS2uNPt459Uvp/s9v5vKJ8pJYjv9PevRNDmlubEXEsXkGZjIsZGGVT0B9+5+teE/tg+F9X8UaJ4bi0nTrrUHjvHaQWsRcoCmMnHSvrsLBwpK7u3q3r+u3psjtqfE0bngX4keLPC/h281b4sfYNH04SpHa3ManzHZsnBVNwx6Hg13+mfFLwxrHiFtDtNVjl1RbdbswbGH7oqrhskYxtZT1715r8Zfhrq+ofs92eg6cZ9X1DTlgkbf8A62ZUHzcE8tg5x3xXmfwv8M+KPEXiDxR4nuNBu9Khj8OyadHHIjK8sot1jUICASTsz7ZrrMz6D0P46+CPEfib+wNP1yGfUyzIibHVJGGchHI2seD0P0rK+KHxy0Pwfa6tpdjqcMniqC0kmgtfLaRVdULYcjgcDoTXzT4Z+F+vafbfDy7Xw1fQX0erSNeyC2YOsayx7C5xwAN2M+9W/Gnw/wDFfhPxp4zgPhCXxDJrrS/YdVjRpDCsjNllI4DbW2kNjoO1AHv3gf472Efwn0nxT4z1C20+e8eWMLCjfOVdgAqDJPArtvCPxQ8M+OdHudT0bVYbm0tf+PhmzGYeM5cMAQMDOTXy742+C/inwx4S+H96+iHxFFpMbi/0mPLgM8pl2kLyVIO0kZxgVY8F/DDXtTuviZp1ho1zoNrqumj7FDIjJCW3K4iDHHHLL7AmgD6D8O/HrwL4s8QLoum69FNqDsUjjaN0WUjsjMAGP0NYPw5+I11qHi7x/HrHiTTrzTNJuCYoo4miazjDMDvZlAPTHU8g14P8N/B/iWbWfDui/wDCtrOzutNud93reoW8ofaJA2/cGUFgOBjd2xWxa/CvxP4gk+M1lb6fc2k2o3Ils2nQxpdBblnKqx4O5R+ooA9o8CfHTRvHHji/0y11vTmtsbbG2VJVmuCOWfcwC/RRk981saz8ePA/h/xMNAv9dhg1EOI3XYxSNj0VnA2g8jqe9fL/AMP/AIb+JvEXjPwdFF4Ok8M/2HIjX2ptG0f2ja+7e2erHGOM9fSs/wAS/C7Xr7Q/HNy3hm/m1OTW42tZfszGRomM+8pxyp+TP4UAfbniLUZNN8N6nf2+0y29pLNHu5G5UJH4cV5R8E/jhN4o+G2p+J/F9zZ2ENldGJpo0KIF2qRxk5OT2rvmtbmT4VPbGKRrttGMflEHeX8jG3Hrmvky28A+J0/ZxutMGhakt+2uxzfZPs7eYUEY+bbjpmgD6OufjpoPiPwH4n1bwnqUV7faTaPP5M0bKVIBKkqwBKkjrVP4P/HKx8VfDWbXvEOo2dpdWDst+VVo0iyTs4OckrjpmvIvCXwn1T/hYnizTrTR59K03UPDxtoZmhZIPNaGLIz0zu3Z981z+n/CXxdqHwc8QeHl8Ny6fq+n6lFdSAAq1+gVwcEnaxTPG3qPegD6i8E/Grwd8Qrm4ttE1mO4uYEMjwyI0T7B1YBgMgeo6VS0/wDaD8A6p4jTQ7bxBC99JIIY8o4jdycBVcjaTnjrXiXw3+E//CfHVtTl0zXvCmu29k9rbLchEtWDxMm1cRq2AT0OTjHJryjxN4S1bwT4T0/TNb8Ow6TqdvqoEGoLjzrpSG3DcCdyqQmPqKAPtzxJ8VPDPhPX9O0TUtSWHVL91jgtlRnYlm2qTgHaCT1NYt5+0N4AsfEJ0WbxDCl4snks2xzEr5xtMgG0c8da8q1D4d33ij9pe4u77S7iTSzpimC+kibyknWBQjBvVWJI968fuvhZ4i0vw54h8P3fgm4n1q3uEuINXhid2kXeFZEIGGUgluPXkUAfZfjj4ueFfhz9mGvaqlpJcDdFEiNI7L/e2qCce9cf8WPivGfhda+IvCPifT7GO4vI4Uv54mkQjDFk2hSVbjuO3vXi/j7wH4j0bxR4W8VXHhSTxfpzaNbW81hNGz+XKsW1ldQCwwTuHGM5pfEnw/8AFE3wCMR8IxaZd3WtRXMel6VBJvEYidd7qXYgk49OMZoA91+I3xq0vwH4dsxJq+nrrt5BHJCsyyNHhgMylUUtt7gHGfWukh+Jvh638DQ+JrrXLSbSfLG++iBCO3QgLyQc/wAPWvmT4leAPEOg+PIvEUvhFvF2l6hpcMCW8kbuIJBbomGVclSrKSOMc9jWfqPwj8XaR8CbCym0q8lu7vXBef2fChd4YvKK5ZRnGSM/j60AfWHgT4neG/iVazzeH9SW9+zkLNGUZHjz0JVgDg+vSuqr5v8AgV4Lv/Cnxz8ZMuj3Gm6JJbhLZvJKQtyhwp6HvX0hQAUUUUAIxxXyj+0N8QLnxl4qXwxpPmXFpZybWjgBYzz45GB1C8j65r6T8a6jd6X4Zvp7CBrnUNmy2iQZLSt8q/hkg/hXJfCX4Q2nw/sje3RW91+6G65um5255Kp6D3718vnOHxGZcuAovli9Zy8uiXdv9NTjxEJ1rUo6Lqzwr9m/UrrTfiJc6Vva1kvrWWFgy8pInzKSp7jDcV2XiPxDNY/GDStK8VxabYWlw0ctzc6fn/TCuRB5u7lVDgHb29TWZb6WdD/auEca7UmuGuAB6SQkn9c10WrfDay+Jvxl8YQ6r5gt7Syt0hkjYqyOwJDD1xg8H1r4nD0cTDBLC0NZwrOKT0i0lzNPydn9/Y+j4VlSo/WFidIRjLVbq7STXzd/l3Peo/u+tO21zngC21XTvDsWn6w3nXlkxtxdDpcIuNkn1K4z7g10lfrFGo6lOM3FptbPdeR5tWCpzcE7pdV18xu0Uu2lorYyE20UtFACbaTaK4/4uePn+Gfge81+OzW/a3eNfIaTYG3OF64Pr6VxPhX9oCfxF4/8OeHW0aOBNW02O/NwLgkxloy+3G3npjOaAPZ9tG2vD/2utc1rRPhnEdJmmtori7WG7mhJDCMg4G4dAWAH44rkfDfxctvgx8H/AAlaWPm+LdY1oNc28IJXZuYbkP3iSrEqAByQelAH09tqNpEjZVZ1Vm4UE4z9K+bdV+PT/Eb4T+PLG40+fw74k0u03yW+9gQN6qSpIDKQTgg+ork7HXLb+zfgc+pQXmoX0kztHcC+KbT9oC/MCp3dR3HTHegD7CXpRgV8v/FL9oafxTb+L/Dmg6TcHTbOznSbWkdlMUijgjaMKCwIBJGaj8N/tAyfDv4U+CtPg0+48R+I9RhldIWkYnb5zKCTyxJOQAPQ0AfUmKjkkSLG51TccDccZPpXjngL9pGx8U+E/Emp6ppk2k3+gRmS7st24kcgbcgEHcCpBHBryXxx8Zk+JWgeGdW1jw1dWcMeveXZi3vWiDgKh3klCG5JHHQjr1oA+wdoryqX9m/wrefEKfxbeSXt7dyXAuhZzyKbdJBjBChc4yAcE1xvxN/aElute17wboWjz3kdvbTxXupozD7OfLbLAKDwDgbiRzXNfDv46f8ACs/gXolxcw3Gu61qF7cxW1u8pJYK/JLHJwMgADPJoA+qwoHFLtFeHfDn9pm28SLr8HiLR5vD2paPateSwsS26NfvDDAEMMrweu4V458WPjlq/wASvDtheW+mXXh+wg1RUtrmOZwblSrbgSAASMDIBPWgD7U2ijFRWZJtISTk7F5/CpqAG7RS4paKAExS0UUAFFFFACMA3WjgLxXG/FDxxceCtGs/7Oto77WdSvI7GxtpW2q8jZOWPoACayYvjJpWn3mu/wBs3lrbWdlqkGlQvCHZjM8aswbjGAxPI4AU5oAxda8P7v2lNBvgvytpckjH3Tcv/s4r0jTvDyWHiTVtVU/NfpCrLjp5YYdfxrjdU+KPga31DStfGo/bp5reSO3ks43l/cGVUd2AHChwoyfwzWy/xd8Mr4kTRPtx+0tcfZPO8tvJFwF3eTvxjfjt/WvKw2BjRnUm+s3NfOKX+ZNHmo86i/i3/B/mjs+BS15f8Pfi1c/EHxxq9jZ2MY8P2sBeC9UkvIwlMeT2AbaxA64XPevUK9UoKKKKACiiigDhfjd4XtvF/wAMdcsLu8/s+FYftButhcR+Wd+So5I+XoK+Xf2cbG51L456a9rqL69Y6XZvGb5Y2SNIxHtVRu5ABbAz1wa+17q1ivbeS3njSaCRSjxyKGVlIwQQeoNUND8L6R4YgaHSNMs9MiY7mS0gWIMfU7QM0AeZftCfC3xP8UbHSrHQ9UhsbBWf7dBcOwWXJUocAHO0g8cda53xR+y7NN4O8K22ga0bLxB4fU+VeSqQkrM29jxyuGyR164r6DooA+dtB/Zn1e18K+MW1TWob/xV4hgMDXRDeVGC6sST1JJUdh0qQfs5639n+Gqf2lZbvC8jNcfK/wC9zMJPk49BjmvoWigD5j8Ufss+I/7c1uPw14misPDuuSb7y0mVtwBYsVOPvAEnHI64NbXjT9mK5k0vwrL4R1r+zNb8PwCCO4uAcS87t+RnadxY9CPmIr6CooA+eNJ/Z18Q2sHxC+1apYyz+J4dsbqH+RzJvJfjvntRrn7OOt6p8P8AwNoCalYpc6DcvNPKyvskDPuwvH86+h6KAPnDx9+zT4ivPGOtat4R8RRaZaa6rLf20wYHDHLgEA5UnnHBGTzV7xL+y0t18PvDek6Nq5tdb0OR54r2RTtld2DNkDlfmCkEdMV9A0UAfPPh79nPXJvFmr6r4n1a3vo9W0g6fdNDu80yGONTJnAHVM1g6R+yb4kbUNN0zWvFMV34S025NxBaxK285OSMHhS3c5PU4r6kooAbGoRQoGABgU6iigAooooAKKKKACiiigDzX4n/AA1ufiB4t8GyyhH0TTJ5ri8TzTG5bZiPbjnr79KqXn7OvhvUHt1upbq4tk1WfVpbeRlKzyyDG1+OVUdP1oooAwrr9maH7VoMNrrDQabpSgRy+VtvR++aUqJVIBU5AwQcAccnNdLp/wAB9HsNUnn+36hcWEmoS6qunSunlR3UgILghdxxkkAng+tFFAGr8M/hTp/wv0+W1sb28vVf5VN2yny0ySFUKAOpPJyT68AV3FFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB//9k="/>
</br>
<h3>Hola ${order.recipient.name},</h3>
<div>
    <div>
        Hemos recibido la solicitud del pedido con los siguientes datos, revise que son correctos o póngase en contacto con nosotros.
    </div>
    <div>
        <h2>Pedido ${order.reference}</h2>
        <p>Realizado el ${order.orderDate?string["EEEE, dd MMMM"]} de ${order.orderDate?string["yyyy"]}</p>
        <table class="list" style="width:50%">
            <tr>
                <th>Libro</th>
                <th>Autor</th>
                <th>Precio</th>
            </tr>
            <#list order.books as book>
            <tr>
                <td>${book.title}</td>
                <td>${book.author}</td>
                <td>${book.price}€</td>
            </tr>
        </#list>
        <tr>
            <td colspan="2">Gastos de envío</td>
            <td>${order.shippingCost}€</td>
        </tr>
        <tr>
            <th colspan="2">Importe total del pedido</th>
            <td>${order.totalPrice}€</td>
        </tr>
        </table>
        <div>
            <h3>Datos de envío</h3>
            <table>
                <tr>
                    <td>${order.recipient.name} ${order.recipient.surname} (${order.recipient.phone})</td>
                </tr>
                <tr>
                    <td>${order.recipient.address.name}</td>
                </tr>
                <tr>
                    <td>${order.recipient.address.postalCode} ${order.recipient.address.city}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
</br>
<div>
    <h3>Información de pago</h3>
    <div>
        Tiene ${params.days} días para realizar la transferencia con los siguientes datos, en caso contrario su pedido se cancelará automáticamente.
    </div>
    <div>
        <table class="bankInfo" style="width:40%">
            <tr>
                <th>Cuenta destino (IBAN):</th>
                <td><strong>${params.account}</strong>
                </th>
            </tr>
            <tr>
                <th>Concepto:
                </td>
                <td><strong>Pedido ${order.id}</strong></td>
            </tr>
            <tr>
                <th>Importe:
                </td>
                <td><strong>${order.totalPrice}€</strong></td>
            </tr>
        </table>
    </div>
</div>
</br>
<div>
    FINSOL le da las gracias por su aportación.
</div>
</body>
</html>
