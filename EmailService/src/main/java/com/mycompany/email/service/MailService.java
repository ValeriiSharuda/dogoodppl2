package com.mycompany.email.service;

import com.mycompany.email.kafka.consumer.Receiver;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Properties;

@Service
@Component
public class MailService {

    @Value(value = "${spring.mail.host}")
    private String host;

    @Value("${spring.mail.port}")
    private int port;

    @Value("${spring.mail.username}")
    private String senderMailAccount;

    @Value("${spring.mail.password}")
    private String password;

    @Autowired
    private JavaMailSender javaMailSender;

    private final Properties properties = new Properties();

    private Session session;

    private Logger log;

    private Configuration freemarker;

    @Autowired
    private Receiver receiver;

    @SneakyThrows
    public void sendEmail(String emailTo, String name, String lastName) {
        //receiver.receive(emailTo,name,lastName);
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(senderMailAccount, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("a.garcia@dogoodpeople.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(emailTo));
            message.setSubject("Subject");
            message.setText("Mensage");

            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }


    @SneakyThrows
    private Template obtainTemplate(String status) throws IOException {
        switch (status) {
            case "NONREGISTER":
                log.info("NonRegister template sent");
                return freemarker.getTemplate("non-register-template.ftl");

            case "REGISTER":
                log.info("Register template sent");
                return freemarker.getTemplate("welcome-template.tfl");

            case "GETPASSWORDBACK":
                log.info("GetPasswordBack template sent");
                return freemarker.getTemplate("get-back-password-template.tlf");
            default:
                return freemarker.getTemplate("non-register-template.ftl");
        }
    }

    @SneakyThrows
    private String getSubject(String status) {

        switch (status) {
            case "NONREGISTER":
                return "Confirmación del registro en DoGood";

            case "REGISTER":
                return "Bienvenido a DoGood";

            case "GETPASSWORDBACK":
                return "Recuperación de password";
            default:
                return "Confirmación del registro en DoGood";
        }
    }
}
