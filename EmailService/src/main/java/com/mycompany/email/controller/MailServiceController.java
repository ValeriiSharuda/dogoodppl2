package com.mycompany.email.controller;

import com.mycompany.email.service.MailService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController(value = "/mailServiceController")
public class MailServiceController {


    @GetMapping
    public ResponseEntity getUserToSendEmail(@RequestParam("email") String email, @RequestParam("name")String name,@RequestParam("lastName") String lastName ) {
        MailService mailService = new MailService();
        mailService.sendEmail(email,name, lastName);
        return new ResponseEntity("200", HttpStatus.OK);
    }

}
