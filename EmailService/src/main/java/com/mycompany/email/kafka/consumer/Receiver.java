package com.mycompany.email.kafka.consumer;

import java.util.concurrent.CountDownLatch;

import com.mycompany.email.service.MailService;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaTemplate;

public class Receiver {
    private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);
    private CountDownLatch latch = new CountDownLatch(1);

    @Autowired
    private MailService emailService;


    @KafkaListener(topics = "${spring.kafka.topic.userRegistered}")
    public void listen(ConsumerRecord<?, ?> cr) throws Exception {
        LOGGER.info(cr.toString());
        latch.countDown();
    }
}
