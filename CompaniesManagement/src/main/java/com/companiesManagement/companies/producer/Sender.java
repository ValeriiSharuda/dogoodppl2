package com.companiesManagement.companies.producer;

import com.companiesManagement.companies.data.DbCompany;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

public class Sender {

    private static final Logger LOGGER = LoggerFactory.getLogger(Sender.class);

    @Autowired
    private KafkaTemplate<String, DbCompany> simpleKafkaTemplate;

    public void send(String topic, DbCompany payload) {
        LOGGER.info("sending payload='{}' to topic='{}'", payload, topic);
        simpleKafkaTemplate.send(topic, payload);
    }
}
