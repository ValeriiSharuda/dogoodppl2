package com.companiesManagement.companies.mapper;

import com.companiesManagement.companies.data.DbCompany;
import com.companiesManagement.companies.model.Company;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class CompanyMapper {

    public Company from(DbCompany dbCompany) {
        Company company = new Company();
        BeanUtils.copyProperties(dbCompany, company);

        return company;
    }

    public DbCompany toDbModel (Company company) {
        DbCompany dbcompany = new DbCompany();
        BeanUtils.copyProperties(company, dbcompany);

        return dbcompany;
    }
}
