package com.companiesManagement.companies.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name ="company")
public class DbCompany {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "companyId")
    private int companyId;

    @Column(name ="companyname")
    private String companyName;

    @Column(name = "cif")
    private String cif;

    @Column(name = "logo")
    private String logo;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "contactpersonname")
    private String contactPersonName;

    @Column(name = "address")
    private String address;

    @Column(name = "postalcode")
    private String postalCode;
}
