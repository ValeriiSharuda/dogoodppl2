package com.companiesManagement.companies.service;

import com.companiesManagement.companies.data.DbCompany;
import com.companiesManagement.companies.mapper.CompanyMapper;
import com.companiesManagement.companies.model.Company;
import com.companiesManagement.companies.producer.Sender;
import com.companiesManagement.companies.repository.CompaniesManagementRepository;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CompaniesManagementService {

    @Autowired
    private CompaniesManagementRepository companiesManagementRepository;

    @Autowired
    private CompanyMapper companyMapper;

    @Autowired
    private Sender sender;

    @Value("${spring.kafka.topic.userRegistered}")
    private String COMPANY_REGISTERED_TOPIC;


    @SneakyThrows
    public Company saveOrUpdate(Company company) {

        DbCompany dbCompany = companyMapper.toDbModel(company);
        DbCompany createdCompany = companiesManagementRepository.save(dbCompany);
        sender.send(COMPANY_REGISTERED_TOPIC, createdCompany);
        return companyMapper.from(dbCompany);
    }

    @SneakyThrows
    public void delete(Company company) {
        companiesManagementRepository.removeByEmail(company.getEmail());
    }
}
