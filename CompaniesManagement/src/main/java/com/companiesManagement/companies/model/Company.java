package com.companiesManagement.companies.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Company {

    private int companyId;
    private String companyName;
    private String cif;
    private String logo;
    private String email;
    private String phone;
    private String contactPersonName;
    private String address;
    private String postalCode;

}
