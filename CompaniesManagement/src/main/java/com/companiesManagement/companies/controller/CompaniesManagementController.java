package com.companiesManagement.companies.controller;

import com.companiesManagement.companies.model.Company;
import com.companiesManagement.companies.service.CompaniesManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value ="/CompaniesController")
public class CompaniesManagementController {

    @Autowired
    private CompaniesManagementService companiesManagementService;

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PostMapping(value = "/insertCompany")
    public ResponseEntity insertUser(@RequestBody Company company) {
        Company newCompany = companiesManagementService.saveOrUpdate(company);
        return new ResponseEntity("Compañia insertada",HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteUser")
    public ResponseEntity deleteUser(@RequestBody Company company) {
        companiesManagementService.delete(company);
        return new ResponseEntity("Compañia borrada",HttpStatus.OK);
    }
}
