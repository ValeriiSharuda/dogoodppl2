package com.companiesManagement.companies.repository;

import com.companiesManagement.companies.data.DbCompany;
import org.springframework.data.repository.CrudRepository;

public interface CompaniesManagementRepository extends CrudRepository<DbCompany,Integer> {

   void removeByEmail(String mail);
}
